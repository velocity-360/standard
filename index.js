import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Elements } from './theme'


const standard = (
    <div data-spy="scroll" data-target=".gd-sidebar" data-offset="100">
        <Elements />
    </div>
)

ReactDOM.render(standard, document.getElementById('root'))