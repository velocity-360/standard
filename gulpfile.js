var gulp = require('gulp')
var minifyCSS = require('gulp-minify-css')
var autoprefixer = require('gulp-autoprefixer')
var gp_concat = require('gulp-concat')
var gp_rename = require('gulp-rename')
var gp_uglify = require('gulp-uglify')

gulp.task('css', function(){
    return gulp.src(
            [
                './assets/css/bootstrap.min.css',
                './assets/plugins/prettify/prettify.css',
                './assets/css/main.css'
            ]
        )
        .pipe(minifyCSS())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gp_concat('style.min.css'))
        .pipe(gulp.dest('./dist/css/'))
})

gulp.task('copy-images', function(){
    return gulp.src(
            ['./images/**']
        )
        .pipe(gulp.dest('./dist/images/'))
})

gulp.task('build', function(){
    return gulp.src(
    		[
				'./assets/js/vendor/modernizr.min.js',
                './assets/js/vendor/jquery.min.js',
                './assets/js/vendor/bootstrap.min.js',
                './assets/plugins/prettify/prettify.js',
                './assets/plugins/jquery.nicescroll.min.js',
                './assets/plugins/clipboard/clipboard.min.js',
                './assets/js/main.js'
    		]
    	)
        .pipe(gp_concat('vendor.min.js'))
        .pipe(gulp.dest('./dist/js/'))
        .pipe(gp_rename('vendor.min.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('./dist/js/'))
})

gulp.task('default', ['build', 'css', 'copy-images'], function(){})
gulp.task('prod', ['build', 'css', 'copy-images'], function(){})
