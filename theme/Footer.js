import React, { Component } from 'react'

export default (props) => {
	return (
		<div>
			<hr />
			<footer className="text-center text-muted">
				<p><small>&copy; 2017 GentoDoc HTML Template. Made with love in Yogyakarta - ID by <a href="http://gentolab.com" target="_blank">GentoLab</a></small></p>
			</footer>
		</div>
	)
}
