import React, { Component } from 'react'

class Section extends Component {
	render(){
		return (
			<section>
				<h2 id="sec7b">Read Me</h2>
				<h3>Support policy:</h3>
				<em><strong>It is important to us to make sure that every customer is satisfied with our templates.</strong></em>
				We’re based in South East Asia, Indonesia (Western Indonesian Time) and we offer support Monday to Friday, 09:00 to 21:00 WIB (GMT+7). Depending on time differences, please allow up to 24 hours for us to get back to you. All the messages sent on weekends will be handled on Monday.
				Support is provided only to verified buyers. Please send message to us via <a href="https://goo.gl/yWlIVx">Profile Page Form</a>.
				<h3>Support for our items includes:</h3>
				<ol>
					<li>Responding to questions or problems regarding the template and its features</li>
					<li>Help working with the template, in case it’s not covered in the documentation</li>
				</ol>
				<h3>Item support does not include:</h3>
				<ol>
					<li>Customization and installation services</li>
					<li>Support for third party software and plug-ins</li>
					<li>Adding new functionality to template</li>
					<li>Fixing errors which may occur after template customization</li>
				</ol>
				<strong>We do not provide after purchase support in any other places like :</strong>
				<ol>
					<li>Twitter</li>
					<li>Facebook</li>
				</ol>
				<h3>Before asking questions</h3>
				Before asking questions, please be sure that you have read the theme documentation which came packaged with your ‘download’
				<h3>Important!</h3>
				If you want a style or function different from what you see in the online demo you need to understand that this is not covered by basic support. We are always here to give small tips and advice (matter of 1-2 minutes), because we like to help. This is a courtesy to our buyers and this service cannot be abused (requests are coming one after the other etc.).

				Please keep in mind that we cannot provide complex customization (beyond 2 – 3 mins). You can consider hiring a freelancer instead. 
				You can check Envato Studio: http://studio.envato.com. There’s plenty of quality freelancers offering customization services.
			</section>
		)
	}
}

export default Section
