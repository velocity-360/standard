import React from 'react'

export default (props) => {
	return (
		<div className="gd-header">

			<div className="header-wrap" style={{marginLeft:0}}>
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-8">
							<div className="item-name">
								<h1><a href="https://goo.gl/uMb0yu" target="_blank">Roosa Template</a></h1>
							</div>
						</div>
						<div className="col-md-4 text-right">
							<a href="#" target="_blank" className="btn btn-success">Buy Now <strong>$20</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>

	)
}