import React, { Component } from 'react'

export default (props) => {
	return (
		<table className="table table-bordered table-hover">
			<tbody>
				<tr>
					<td style={{width: 50}}>01</td>
					<td>Bootstrap</td>
					<td><a href="http://getboostrap.com">http://getboostrap.com</a></td>
				</tr>
				<tr>
					<td>02</td>
					<td>HTML5 Boilerplate</td>
					<td><a href="https://html5boilerplate.com/">https://html5boilerplate.com/</a></td>
				</tr>
				<tr>
					<td>03</td>
					<td>jQuery</td>
					<td><a href="https://jquery.com/">https://jquery.com/</a></td>
				</tr>
				<tr>
					<td>04</td>
					<td>Modernizr</td>
					<td><a href="https://modernizr.com/">https://modernizr.com/</a></td>
				</tr>
				<tr>
					<td>05</td>
					<td>LESS</td>
					<td><a href="http://lesscss.org/">http://lesscss.org/</a></td>
				</tr>
				<tr>
					<td>06</td>
					<td>Prettify</td>
					<td><a href="https://github.com/google/code-prettify">https://github.com/google/code-prettify</a></td>
				</tr>
				<tr>
					<td>07</td>
					<td>Raphael Js</td>
					<td><a href="http://dmitrybaranovskiy.github.io/raphael/">http://dmitrybaranovskiy.github.io/raphael/</a></td>
				</tr>
				<tr>
					<td>08</td>
					<td>Nicescroll</td>
					<td><a href="https://areaaperta.com/nicescroll/">https://areaaperta.com/nicescroll/</a></td>
				</tr>
				<tr>
					<td>09</td>
					<td>isMobile</td>
					<td><a href="https://github.com/kaimallea/isMobile">https://github.com/kaimallea/isMobile</a></td>
				</tr>
				<tr>
					<td>10</td>
					<td>BlockUI</td>
					<td><a href="http://malsup.com/jquery/block/">http://malsup.com/jquery/block/</a></td>
				</tr>
				<tr>
					<td>11</td>
					<td>Clipboard</td>
					<td><a href="https://clipboardjs.com/">https://clipboardjs.com/</a></td>
				</tr>
				<tr>
					<td>12</td>
					<td>Filestyle</td>
					<td><a href="http://markusslima.github.io/bootstrap-filestyle/">http://markusslima.github.io/bootstrap-filestyle/</a></td>
				</tr>
				<tr>
					<td>13</td>
					<td>Pace</td>
					<td><a href="http://github.hubspot.com/pace/">http://github.hubspot.com/pace/</a></td>
				</tr>
				<tr>
					<td>14</td>
					<td>Glyphicons</td>
					<td><a href="http://getbootstrap.com/components/#glyphicons">http://getbootstrap.com/components/#glyphicons</a></td>
				</tr>
				<tr>
					<td>15</td>
					<td>http://fontawesome.io/</td>
					<td><a href="">http://fontawesome.io/</a></td>
				</tr>
				<tr>
					<td>16</td>
					<td>Entypo</td>
					<td><a href="http://entypo.com/">http://entypo.com/</a></td>
				</tr>
				<tr>
					<td>17</td>
					<td>Flag Icon</td>
					<td><a href="http://flag-icon-css.lip.is/">http://flag-icon-css.lip.is/</a></td>
				</tr>
				<tr>
					<td>18</td>
					<td>Material Icon</td>
					<td><a href="https://material.io/icons/">https://material.io/icons/</a></td>
				</tr>
				<tr>
					<td>19</td>
					<td>Weather Icon</td>
					<td><a href="https://erikflowers.github.io/weather-icons/">https://erikflowers.github.io/weather-icons/</a></td>
				</tr>
				<tr>
					<td>20</td>
					<td>Backstretch</td>
					<td><a href="http://srobbin.com/jquery-plugins/backstretch/">http://srobbin.com/jquery-plugins/backstretch/</a></td>
				</tr>
				<tr>
					<td>21</td>
					<td>Bootstrap Switch</td>
					<td><a href="https://github.com/nostalgiaz/bootstrap-switch">https://github.com/nostalgiaz/bootstrap-switch</a></td>
				</tr>
				<tr>
					<td>22</td>
					<td>Chart Js</td>
					<td><a href="http://www.chartjs.org/">http://www.chartjs.org/</a></td>
				</tr>
				<tr>
					<td>23</td>
					<td>Cleave</td>
					<td><a href="https://nosir.github.io/cleave.js/">https://nosir.github.io/cleave.js/</a></td>
				</tr>
				<tr>
					<td>24</td>
					<td>jQuery Datatable</td>
					<td><a href="https://datatables.net/">https://datatables.net/</a></td>
				</tr>
				<tr>
					<td>25</td>
					<td>Bootstrap Datepicker</td>
					<td><a href="https://bootstrap-datepicker.readthedocs.io/en/latest/">https://bootstrap-datepicker.readthedocs.io/en/latest/</a></td>
				</tr>
				<tr>
					<td>26</td>
					<td>Easy Pie</td>
					<td><a href="https://rendro.github.io/easy-pie-chart/">https://rendro.github.io/easy-pie-chart/</a></td>
				</tr>
				<tr>
					<td>27</td>
					<td>Masked Input</td>
					<td><a href="http://digitalbush.com/projects/masked-input-plugin/">http://digitalbush.com/projects/masked-input-plugin/</a></td>
				</tr>
				<tr>
					<td>28</td>
					<td>Max Length</td>
					<td><a href="http://mimo84.github.io/bootstrap-maxlength/">http://mimo84.github.io/bootstrap-maxlength/</a></td>
				</tr>
				<tr>
					<td>29</td>
					<td>Medium Editor</td>
					<td><a href="https://github.com/yabwe/medium-editor">https://github.com/yabwe/medium-editor</a></td>
				</tr>
				<tr>
					<td>30</td>
					<td>Morris Chart</td>
					<td><a href="http://morrisjs.github.io/morris.js/">http://morrisjs.github.io/morris.js/</a></td>
				</tr>
				<tr>
					<td>31</td>
					<td>Selectize</td>
					<td><a href="http://selectize.github.io/selectize.js/">http://selectize.github.io/selectize.js/</a></td>
				</tr>
				<tr>
					<td>32</td>
					<td>Sparkline</td>
					<td><a href="http://omnipotent.net/jquery.sparkline/#s-about">http://omnipotent.net/jquery.sparkline/#s-about</a></td>
				</tr>
				<tr>
					<td>33</td>
					<td>Stepy</td>
					<td><a href="https://github.com/wbotelhos/stepy">https://github.com/wbotelhos/stepy</a></td>
				</tr>
				<tr>
					<td>34</td>
					<td>Summernote</td>
					<td><a href="http://summernote.org/">http://summernote.org/</a></td>
				</tr>
				<tr>
					<td>35</td>
					<td>Bootstrap Validator</td>
					<td><a href="https://1000hz.github.io/bootstrap-validator/">https://1000hz.github.io/bootstrap-validator/</a></td>
				</tr>
				<tr>
					<td>36</td>
					<td>jQuery Validate</td>
					<td><a href="https://jqueryvalidation.org/">https://jqueryvalidation.org/</a></td>
				</tr>
			</tbody>
		</table>
	)

}
