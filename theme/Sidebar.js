import React from 'react'

export default (props) => {
	return (
		<div className="gd-sidebar">
			<ul className="nav nav-stacked" id="sidebar">
				<li>
					<a href="#sec1">
					Welcome
					</a>
				</li>
				<li>
					<a href="#sec2">
					Intro
					</a>
					<ul>
						<li><a href="#sec2">About</a></li>
						<li><a href="#sec2a">Whats Included</a></li>
						<li><a href="#sec2b">Changelogs</a></li>
					</ul>
				</li>
				<li>
					<a href="#sec3">
					Getting Started
					</a>
					<ul>
						<li><a href="#sec3">Preparation</a></li>
						<li><a href="#sec3a">Installation</a></li>
						<li><a href="#sec3b">Basic Template</a></li>
					</ul>
				</li>
				<li>
					<a href="#sec4">
					Layouts
					</a>
					<ul>
						<li><a href="#sec4a">Creating First Page</a></li>
						<li><a href="#sec4b">Page Layouts</a></li>
						<li><a href="#sec4c">Layout Variations</a></li>
					</ul>
				</li>
				<li>
					<a href="#sec6">
					Item Update
					</a>
					<ul>
						<li><a href="#sec6">How to Update</a></li>
						<li><a href="#sec6a">Update Patch</a></li>
					</ul>
				</li>
				<li>
					<a href="#sec7">
					More Info
					</a>
					<ul>
						<li><a href="#sec7">Rate this item!</a></li>
						<li><a href="#sec7a">Credit</a></li>
						<li><a href="#sec7b">Read me</a></li>
					</ul>
				</li>
				<li>
					<a href="#sec8">
					Keep in Touch
					</a>
				</li>
			</ul>
		</div>
	)

}