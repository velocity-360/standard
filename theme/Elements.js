import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'
import Table from './Table'
import Footer from './Footer'
import Section from './Section'

export default (props) => {

	return (
		<div className="gd-wrapper">
			<Nav />
			<Sidebar />

			<div className="gd-content">
				<div className="inner-content">
					<div className="container-fluid">

						<section>
							<div style={{height:30, display:'block'}} id="sec1"></div>

							<div className="alert alert-info text-center">
								<strong>Note :</strong> This documentation page is not device friendly (non responsive design), please access this page using your PCs or Laptops.
							</div>

							<div className="well dashed-well text-center">
								<h3><strong>Welcome! Have a good day!</strong></h3>
								<p>We are so excited and thanks for purchasing our item. In this page, you will find some guides related to the item that you have purchased. We will not write too much text on this page, we will provide more images with short information so that it easier to understand. We will also continue to provide information on any updates of the item which contain new features and bug fixes.</p>
								<p>If you have any questions that are beyond the scope of this documentation file, please feel free to email us via our user page <strong><a href="https://goo.gl/yWlIVx">contact form here</a></strong>. Thanks again!</p>
							</div>

							<images src="assets/images/roosa-main-images-demo.jpg" className="images-responsive" />

							<div className="row">
								<div className="col-md-3">
									<div className="well text-center">
										<p>Item Release</p>
										<h4 className="text-success"><strong>10 Jan 2017</strong></h4>
									</div>
								</div>
								<div className="col-md-3">
									<div className="well text-center">
										<p>Last Update</p>
										<h4 className="text-success"><strong>16 Jan 2017</strong></h4>
									</div>
								</div>
								<div className="col-md-3">
									<div className="well text-center">
										<p>Current Version</p>
										<h4 className="text-success"><strong>1.1.0</strong></h4>
									</div>
								</div>
								<div className="col-md-3">
									<div className="well text-center">
										<p>Marketplace</p>
										<h4 className="text-success"><strong>ThemeForest</strong></h4>
									</div>
								</div>
							</div>
							<a href="#sec4" className="btn btn-default btn-block">VIEW CHANGELOG</a>
						</section>

						<section>
							<h2 id="sec2">Introduction</h2>
							<h3>About</h3>
							<p><strong>Roosa</strong> is a HTML admin or dashboard template powered with <strong>Twitter Bootstrap 3.3.7</strong> framework & <strong>HTML5 Boilerplate</strong>, both are the perfect combination to start in making a website template. Roosa can be used for any type of web applications like Blogging Dashboard, Monitoring Dashboard, Shop Cak End etc. Roosa has a modern flat design which makes your next project look awesome and yet user-friendly. Roosa has 20+ collection of jQuery plugins and UI components and support on all major web browsers, tablets and phones.Roosa comes with <strong>4 layout variations</strong>. Once you purchased Roosa, you will be entitled to free download of all future updates for the same license.</p>
							<p>Here are some key features of <strong>Roosa</strong> :</p>
							<br />
							<div className="well">
								<div className="row">
									<div className="col-sm-4">
										<ul>
											<li>Bootstrap 3.x.x based</li>
											<li>HTML5 Boilerplate</li>
											<li>4 Layout Variations</li>
										</ul>
									</div>
									<div className="col-sm-4">
										<ul>
											<li>Responsive Design</li>
											<li>Build with LESS</li>
											<li>20+ jQuery Plugins</li>
										</ul>
									</div>
									<div className="col-sm-4">
										<ul>
											<li>2000+ Font Icons</li>
											<li>10+ Ready Pages</li>
											<li>Fixed &amp; Static Layout</li>
										</ul>
									</div>
								</div>
							</div>
						</section>

						<section>
							<h2 id="sec3">Getting Started</h2>
							<h3>Preparation</h3>
							<p>
							Make sure youve set up multiple application support for developing and further editing the template. All you need to do so is a code editor like Sublime, Notepad ++, Atom or other code editor application according to your taste. In this case, we use the Sublime when doing development.
							Then, of course, you also have to have a Web browser that supports the latest web technologies like CSS3 and HTML5.
							</p>
							<ol>
								<li><a href="https://www.sublimetext.com/">Sublime</a></li>
								<li><a href="https://notepad-plus-plus.org/">Notepad++</a></li>
								<li><a href="https://atom.io/">Atom</a></li>
								<li><a href="https://www.google.com/chrome/browser/desktop/">Google Chrome</a></li>
								<li><a href="https://www.apachefriends.org/download.html">XAMPP</a></li>
							</ol>
						</section>

						<section>
							<h2 id="sec3a">Installation</h2>
								<p>
								Installing HTML template is very easy, you do not need to do any configuration. The easiest way is to access via <code>file://</code> or by double clicking on the HTML file. But sometimes the way it will make the most of the jQuery plugins not running properly, then it is good to have a local server application on your computer such as <i>XAMPP</i>. What you should do after installing a local server application is to copy all of the assets templates into the htdocs folder and access them through a web browser such as <i>Google Chrome</i>.
								</p>
						</section>

						<section>
							<h2 id="sec3b">Basic Template</h2>
								<p>
								Start with this basic HTML template, or modify the starter pages. We hope youll customize our templates and starter pages, adapting them to suit your needs.
								</p>
								<p>Copy the HTML below to begin working with a minimal HTML document.</p>
								<div className="pre-wrapper">
								<button className="btn"  data-clipboard-target="#pre-basic-html">Copy</button>
	<pre className="prettyprint linenums " id="pre-basic-html">
&lt;!DOCTYPE html&gt;
&lt;html lang=en&gt;

&lt;head&gt;
	&lt;meta charset=&quot;utf-8&quot;&gt;
	&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;&gt;
	&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1&quot;&gt;
	&lt;meta name=&quot;description&quot; content=&quot;&quot;&gt;
	&lt;meta name=&quot;author&quot; content=&quot;&quot;&gt;
	&lt;title&gt;Roosa - Just Another Dashboard Template&lt;/title&gt;

	&lt;!-- Fonts --&gt;
	&lt;link href=&quot;https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&quot; rel=&quot;stylesheet&quot;&gt;

	&lt;!-- Framework --&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/vendor/bootstrap.min.css&quot;&gt;

	&lt;!-- Main Template CSS --&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/bootstrap.custom.css&quot;&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/style.css&quot;&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/helper.css&quot;&gt;
&lt;/head&gt;


&lt;body&gt;

	&lt;div id=&quot;rs-wrapper&quot;&gt;

		&lt;h1&gt;Hello World&lt;/h1&gt;

	&lt;/div&gt;&lt;!-- /#rs-wrapper --&gt;

	
	&lt;script src=&quot;dist/js/vendor/modernizr.min.js&quot;&gt;&lt;/script&gt;
	&lt;script src=&quot;dist/js/vendor/jquery.min.js&quot;&gt;&lt;/script&gt;
	&lt;script src=&quot;dist/js/vendor/bootstrap.min.js&quot;&gt;&lt;/script&gt;

	&lt;!-- Template Js --&gt;
	&lt;script src=&quot;dist/js/apps.js&quot;&gt;&lt;/script&gt;

&lt;/body&gt;
&lt;/html&gt;</pre>
								</div>
						</section>

						<section>
							<h2 id="sec4c">Layout Variations</h2>
							
								<h3>Default Sidebar Layout</h3>
								<p>The example above are using default sidebar layout, you only need to call Javascript file into your HTML page to use this layout variation.</p>
								<p><a href="https://goo.gl/XZVGVZ" target="_blank" className="btn btn-success">Template Demo</a></p>
								<div className="pre-wrapper">
								<button className="btn"  data-clipboard-target="#pre-js-default-layout">Copy</button>
<pre className="prettyprint linenums scrollable" id="pre-js-default-layout">
&lt;!-- Additional JS File --&gt;
&lt;script src=&quot;dist/js/layout-default.js&quot;&gt;&lt;/script&gt;</pre>
								</div>
							
								<h3>Mini Sidebar Layout</h3>
								<p>The second variation is mini sidebar layout, this layout presented small sidebar size with iconic menu. The text menu and sub menu will show when we hover one of the icons.</p>
								<div className="alert alert-warning">
								The mini sidebar only works on desktop devices with minimum width is <code>768px</code>, under that width or opened in mobile device (such as Ipad, Tablets or Phones) in any resolutions, the sidebar automatically change to default size. Check the live demo with your mobile devices and the desktop too see the difference.
								</div>
								<p><a href="https://goo.gl/xZ8lFn" target="_blank" className="btn btn-success">Template Demo</a></p>
								<div className="pre-wrapper">
								<button className="btn"  data-clipboard-target="#pre-js-mini-layout">Copy</button>
<pre className="prettyprint linenums scrollable" id="pre-js-mini-layout">
&lt;!-- Additional CSS File --&gt;
&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/sidebar-mini.css&quot;&gt;

&lt;!-- Additional JS File --&gt;
&lt;script src=&quot;dist/js/layout-mini-sidebar.js&quot;&gt;&lt;/script&gt;</pre>
								</div>
							
								<h3>Iconic Sidebar Layout</h3>
								<p>The third variation is iconic sidebar layout, this layout presented small sidebar size with iconic menu. The text menu show as tooltips, and its not support multilevel menu. It means this variation presented simplicity template for simple web app.</p>
								<div className="alert alert-warning">
								The sidebar nav will move to bottom on device under <code>738px</code>, check this template on your mobile devices.
								</div>
								<p><a href="https://goo.gl/ZdTMXL" target="_blank" className="btn btn-success">Template Demo</a></p>
								<div className="pre-wrapper">
								<button className="btn"  data-clipboard-target="#pre-js-iconic-layout">Copy</button>
<pre className="prettyprint linenums scrollable" id="pre-js-iconic-layout">
&lt;!-- Additional CSS File --&gt;
&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/sidebar-iconic.css&quot;&gt;

&lt;!-- Additional JS File --&gt;
&lt;script src=&quot;dist/js/layout-icon-sidebar.js&quot;&gt;&lt;/script&gt;</pre>
								</div>
							
								<h3>Horizontal Menu Layout</h3>
								<p>The last variation is horizontal menu layout, this layout converts default sidebar into horizontal top menu.</p>
								<div className="alert alert-warning">
								The horizontal menu only works on desktop devices with minimum width is <code>1200px</code>, under that width or opened in mobile device (such as Ipad, Tablets or Phones) in any resolutions, the sidebar automatically change to default size. Check the live demo with your mobile devices and the desktop too see the difference.
								</div>
								<p><a href="https://goo.gl/aCVbSQ" target="_blank" className="btn btn-success">Template Demo</a></p>
								<div className="pre-wrapper">
								<button className="btn"  data-clipboard-target="#pre-js-mini-layout">Copy</button>
<pre className="prettyprint linenums scrollable" id="pre-js-mini-layout">
&lt;!-- Additional CSS File --&gt;
&lt;link rel=&quot;stylesheet&quot; href=&quot;dist/css/horizontal-menu.css&quot;&gt;

&lt;!-- Additional JS File --&gt;
&lt;script src=&quot;dist/js/layout-horizontal.js&quot;&gt;&lt;/script&gt;</pre>
								</div>
						</section>

						<section>
							<h2 id="sec7a">Credit</h2>
							<p>We use all the resources below to create Roosa, combine all into a template that you can use to start work on the project website. We are very grateful to the creator of all the resources below so we can spawn this template. We greatly appreciate the license on any existing resource.<br /><br /></p>
							<Table />
						</section>

						<Section />

						<Footer />							
					</div>
				</div>
			</div>
		</div>
		
	)

}