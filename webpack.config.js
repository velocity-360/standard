var webpack = require('webpack')
var path = require('path')

module.exports = {
	entry: {
		app: './index.js'
	},
	output: {
		path: __dirname+'/dist',
		filename: 'bundle/[name].js',
        sourceMapFilename: 'bundle/[name].map'
	},
	devtool: '#source-map',	
	plugins: process.env.NODE_ENV === 'production' ? [
    	new webpack.optimize.UglifyJsPlugin({
    		minimize: true,
		    compress: {
		        warnings: false,
		        drop_console: true
		    }
    	})
	] : [],	
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query:{
					presets:['react', 'es2015']
				}
			}
		]
	}
}

